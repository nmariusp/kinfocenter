# translation of kcmdevinfo.po to Khmer
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Khoem Sokhem <khoemsokhem@khmeros.info>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: kcmdevinfo\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-16 00:48+0000\n"
"PO-Revision-Date: 2011-05-06 11:46+0700\n"
"Last-Translator: Khoem Sokhem <khoemsokhem@khmeros.info>\n"
"Language-Team: Khmer <support@khmeros.info>\n"
"Language: km\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: KBabel 1.11.4\n"
"X-Language: km-KH\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ខឹម សុខែម, ម៉ន ម៉េត, សេង សុត្ថា, ចាន់ សម្បត្តិរតនៈ, សុខ សុភា"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"khoemsokhem@khmeros.info,​​mornmet@khmeros.info,sutha@khmeros.info,"
"ratanak@khmeros.info,sophea@khmeros.info"

#: devicelisting.cpp:41
#, kde-format
msgctxt "Device Listing Whats This"
msgid "Shows all the devices that are currently listed."
msgstr "បង្ហាញ​ឧបករណ៍​ទាំងអស់​ ដែល​បច្ចុប្បន្ន​ត្រូវ​បាន​រាយ ។"

#: devicelisting.cpp:44
#, kde-format
msgid "Devices"
msgstr "ឧបករណ៍"

#: devicelisting.cpp:57
#, kde-format
msgid "Collapse All"
msgstr "វេញ​ទាំងអស់"

#: devicelisting.cpp:60
#, kde-format
msgid "Expand All"
msgstr "ពង្រីក​ទាំងអស់"

#: devicelisting.cpp:63
#, kde-format
msgid "Show All Devices"
msgstr "បង្ហាញ​ឧបករណ៍​ទាំងអស់"

#: devicelisting.cpp:66
#, kde-format
msgid "Show Relevant Devices"
msgstr "បង្ហាញ​ឧបករណ៍​ដែល​ទាក់ទង"

#: devicelisting.cpp:95
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown device type"
msgid "Unknown"
msgstr "មិន​ស្គាល់"

#: devicelisting.cpp:136 devinfo.cpp:79
#, fuzzy, kde-format
#| msgid "None"
msgctxt "no device UDI"
msgid "None"
msgstr "គ្មាន"

#: devinfo.cpp:28
#, kde-format
msgid "kcmdevinfo"
msgstr "kcmdevinfo"

#: devinfo.cpp:28
#, fuzzy, kde-format
#| msgid "KDE Solid Based Device Viewer"
msgid "Device Viewer"
msgstr "កម្មវិធី​មើល​ឧបករណ៍​ដែល​មាន​មូលដ្ឋាន​លើ KDE"

#: devinfo.cpp:28
#, kde-format
msgid "(c) 2010 David Hubner"
msgstr "រក្សាសិទ្ធិ​ឆ្នាំ ២០១០ ដោយ David Hubner"

#: devinfo.cpp:58
#, kde-format
msgid "UDI: "
msgstr "UDI ៖ "

#: devinfo.cpp:66
#, kde-format
msgctxt "Udi Whats This"
msgid "Shows the current device's UDI (Unique Device Identifier)"
msgstr "បង្ហាញ​ UDI (Unique Device Identifier) របស់​ឧបករណ៍​បច្ចុប្បន្ន"

#: infopanel.cpp:20
#, kde-format
msgid "Device Information"
msgstr "ព័ត៌មាន​ឧបករណ៍"

#: infopanel.cpp:29
#, kde-format
msgctxt "Info Panel Whats This"
msgid "Shows information about the currently selected device."
msgstr "បង្ហាញ​ព័ត៌មាន​អំពី​ឧបករណ៍​ដែល​បាន​ជ្រើស​បច្ចុប្បន្ន ។"

#: infopanel.cpp:56
#, kde-format
msgid ""
"\n"
"Solid Based Device Viewer Module"
msgstr ""
"\n"
"ម៉ូឌុល​កម្មវិធី​មើល​ឧបករណ៍​ដែល​មាន​មូលដ្ឋនា​លើ​ Solid"

#: infopanel.cpp:119
#, kde-format
msgid "Description: "
msgstr ""

#: infopanel.cpp:121
#, kde-format
msgid "Product: "
msgstr "ផលិតផល ៖"

#: infopanel.cpp:123
#, kde-format
msgid "Vendor: "
msgstr "ក្រុមហ៊ុន​លក់ ៖"

#: infopanel.cpp:145
#, kde-format
msgid "Yes"
msgstr "បាទ/ចាស"

#: infopanel.cpp:147
#, kde-format
msgid "No"
msgstr "ទេ"

#: infopanel.h:36
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "name of something is not known"
msgid "Unknown"
msgstr "មិន​ស្គាល់"

#: soldevice.cpp:65
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown device"
msgid "Unknown"
msgstr "មិន​ស្គាល់"

#: soldevice.cpp:91
#, kde-format
msgctxt "Default device tooltip"
msgid "A Device"
msgstr "ឧបករណ៍"

#: soldevicetypes.cpp:43
#, kde-format
msgid "Processors"
msgstr "ប្រព័ន្ធ​ដំណើរការ"

#: soldevicetypes.cpp:59
#, kde-format
msgid "Processor %1"
msgstr "ប្រព័ន្ធ​ដំណើរការ %1"

#: soldevicetypes.cpp:76
#, kde-format
msgid "Intel MMX"
msgstr "Intel MMX"

#: soldevicetypes.cpp:79
#, kde-format
msgid "Intel SSE"
msgstr "Intel SSE"

#: soldevicetypes.cpp:82
#, kde-format
msgid "Intel SSE2"
msgstr "Intel SSE2"

#: soldevicetypes.cpp:85
#, kde-format
msgid "Intel SSE3"
msgstr "Intel SSE3"

#: soldevicetypes.cpp:88
#, fuzzy, kde-format
#| msgid "Intel SSE3"
msgid "Intel SSSE3"
msgstr "Intel SSE3"

#: soldevicetypes.cpp:91
#, fuzzy, kde-format
#| msgid "Intel SSE4"
msgid "Intel SSE4.1"
msgstr "Intel SSE4"

#: soldevicetypes.cpp:94
#, fuzzy, kde-format
#| msgid "Intel SSE4"
msgid "Intel SSE4.2"
msgstr "Intel SSE4"

#: soldevicetypes.cpp:97
#, fuzzy, kde-format
#| msgid "AMD 3DNow"
msgid "AMD 3DNow!"
msgstr "AMD 3DNow"

#: soldevicetypes.cpp:100
#, kde-format
msgid "ATI IVEC"
msgstr "ATI IVEC"

#: soldevicetypes.cpp:103
#, fuzzy, kde-format
#| msgid "None"
msgctxt "no instruction set extensions"
msgid "None"
msgstr "គ្មាន"

#: soldevicetypes.cpp:106
#, kde-format
msgid "Processor Number: "
msgstr "លេខ​​ប្រព័ន្ធ​ដំណើរការ ៖"

#: soldevicetypes.cpp:106
#, kde-format
msgid "Max Speed: "
msgstr "ល្បឿន​អតិបរមា ៖"

#: soldevicetypes.cpp:107
#, kde-format
msgid "Supported Instruction Sets: "
msgstr "សំណុំ​សេចក្ដី​ណែនាំ​ដែល​បានគាំទ្រ ៖"

#: soldevicetypes.cpp:132
#, kde-format
msgid "Storage Drives"
msgstr "ឧបករណ៍​ផ្ទុក"

#: soldevicetypes.cpp:151
#, kde-format
msgid "Hard Disk Drive"
msgstr "ដ្រាយ​ថាស​រឹង"

#: soldevicetypes.cpp:154
#, kde-format
msgid "Compact Flash Reader"
msgstr "កម្មវិធី​អាន Compact Flash"

#: soldevicetypes.cpp:157
#, kde-format
msgid "Smart Media Reader"
msgstr "កម្មវិធី​អាន​មេឌៀ​ឆ្លាត​វៃ"

#: soldevicetypes.cpp:160
#, kde-format
msgid "SD/MMC Reader"
msgstr "កម្មវិធី​អាន SD/MMC"

#: soldevicetypes.cpp:163
#, kde-format
msgid "Optical Drive"
msgstr "ដ្រាយ​អ៊ុបទិច"

#: soldevicetypes.cpp:166
#, kde-format
msgid "Memory Stick Reader"
msgstr "កម្មវិធី​អាន​ឧបករណ៍​ផ្ទុក​ចល័ត"

#: soldevicetypes.cpp:169
#, kde-format
msgid "xD Reader"
msgstr "កម្មវិធី​អាន xD"

#: soldevicetypes.cpp:172
#, kde-format
msgid "Unknown Drive"
msgstr "មិន​ស្គាល់​ដ្រាយ"

#: soldevicetypes.cpp:192
#, kde-format
msgid "IDE"
msgstr "IDE"

#: soldevicetypes.cpp:195
#, kde-format
msgid "USB"
msgstr "USB"

#: soldevicetypes.cpp:198
#, kde-format
msgid "IEEE1394"
msgstr "IEEE1394"

#: soldevicetypes.cpp:201
#, kde-format
msgid "SCSI"
msgstr "SCSI"

#: soldevicetypes.cpp:204
#, kde-format
msgid "SATA"
msgstr "SATA"

#: soldevicetypes.cpp:207
#, fuzzy, kde-format
#| msgid "Platform"
msgctxt "platform storage bus"
msgid "Platform"
msgstr "វេទិកា"

#: soldevicetypes.cpp:210
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown storage bus"
msgid "Unknown"
msgstr "មិន​ស្គាល់"

#: soldevicetypes.cpp:213
#, kde-format
msgid "Bus: "
msgstr "ខ្សែបញ្ជូន ៖"

#: soldevicetypes.cpp:213
#, kde-format
msgid "Hotpluggable?"
msgstr "អាច​ដោត​ហើយ​ដើរ ?"

#: soldevicetypes.cpp:213
#, kde-format
msgid "Removable?"
msgstr "អាច​យក​ចេញ​បាន ?"

#: soldevicetypes.cpp:257
#, kde-format
msgid "Unused"
msgstr "មិន​បាន​ប្រើ"

#: soldevicetypes.cpp:260
#, kde-format
msgid "File System"
msgstr "ប្រព័ន្ធ​ឯកសារ"

#: soldevicetypes.cpp:263
#, kde-format
msgid "Partition Table"
msgstr "តារាង​ភាគ​ថាស"

#: soldevicetypes.cpp:266
#, kde-format
msgid "Raid"
msgstr "Raid"

#: soldevicetypes.cpp:269
#, kde-format
msgid "Encrypted"
msgstr "បាន​អ៊ីនគ្រិប"

#: soldevicetypes.cpp:272
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown volume usage"
msgid "Unknown"
msgstr "មិន​ស្គាល់"

#: soldevicetypes.cpp:275
#, kde-format
msgid "File System Type: "
msgstr "ប្រភេទ​ប្រព័ន្ធ​ឯកសារ ៖"

#: soldevicetypes.cpp:275
#, kde-format
msgid "Label: "
msgstr "ស្លាក ៖"

#: soldevicetypes.cpp:276
#, kde-format
msgid "Not Set"
msgstr "មិន​បាន​កំណត់ ៖"

#: soldevicetypes.cpp:276
#, kde-format
msgid "Volume Usage: "
msgstr "ការ​ប្រើប្រាស់​កម្រិត​សំឡេង ៖"

#: soldevicetypes.cpp:276
#, kde-format
msgid "UUID: "
msgstr "UUID ៖ "

#: soldevicetypes.cpp:282
#, kde-format
msgid "Mounted At: "
msgstr "បាន​ម៉ោន​នៅ ៖"

#: soldevicetypes.cpp:282
#, kde-format
msgid "Not Mounted"
msgstr "មិន​បានម៉ោន​ទេ"

#: soldevicetypes.cpp:287
#, kde-format
msgid "Volume Space:"
msgstr "ចន្លោះ​កម្រិត​សំឡេង ៖"

#: soldevicetypes.cpp:297
#, kde-format
msgctxt "Available space out of total partition size (percent used)"
msgid "%1 free of %2 (%3% used)"
msgstr "%1 ទំនេរ នៃ %2 (%3% បាន​ប្រើ)"

#: soldevicetypes.cpp:303
#, kde-format
msgid "No data available"
msgstr ""

#: soldevicetypes.cpp:330
#, kde-format
msgid "Multimedia Players"
msgstr "កម្មវិធី​ពហុមេឌៀ"

#: soldevicetypes.cpp:349 soldevicetypes.cpp:388
#, kde-format
msgid "Supported Drivers: "
msgstr "កម្មវិធី​បញ្ជា​ដែល​បានគាំទ្រ ៖"

#: soldevicetypes.cpp:349 soldevicetypes.cpp:388
#, kde-format
msgid "Supported Protocols: "
msgstr "ពិធីការ​ដែល​បានគាំទ្រ ៖"

#: soldevicetypes.cpp:369
#, kde-format
msgid "Cameras"
msgstr "ម៉ាស៊ីន​ថត"

#: soldevicetypes.cpp:408
#, kde-format
msgid "Batteries"
msgstr "ថ្ម"

#: soldevicetypes.cpp:430
#, kde-format
msgid "PDA"
msgstr "PDA"

#: soldevicetypes.cpp:433
#, kde-format
msgid "UPS"
msgstr "យូភីអេស"

#: soldevicetypes.cpp:436
#, kde-format
msgid "Primary"
msgstr "ចម្បង"

#: soldevicetypes.cpp:439
#, kde-format
msgid "Mouse"
msgstr "កណ្ដុរ"

#: soldevicetypes.cpp:442
#, kde-format
msgid "Keyboard"
msgstr "ក្ដារចុច"

#: soldevicetypes.cpp:445
#, kde-format
msgid "Keyboard + Mouse"
msgstr "ក្ដារចុច+កណ្ដុរ"

#: soldevicetypes.cpp:448
#, kde-format
msgid "Camera"
msgstr "ម៉ាស៊ីន​ថត"

#: soldevicetypes.cpp:451
#, kde-format
msgid "Phone"
msgstr ""

#: soldevicetypes.cpp:454
#, kde-format
msgctxt "Screen"
msgid "Monitor"
msgstr ""

#: soldevicetypes.cpp:457
#, kde-format
msgctxt "Wireless game pad or joystick battery"
msgid "Gaming Input"
msgstr ""

#: soldevicetypes.cpp:460
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown battery type"
msgid "Unknown"
msgstr "មិន​ស្គាល់"

#: soldevicetypes.cpp:466
#, kde-format
msgid "Charging"
msgstr "កំពុង​បញ្ចូល​ថ្ម"

#: soldevicetypes.cpp:469
#, kde-format
msgid "Discharging"
msgstr "មិន​កំពុង​បញ្ចូល​ថ្ម"

#: soldevicetypes.cpp:472
#, fuzzy, kde-format
#| msgid "No Charge"
msgid "Fully Charged"
msgstr "មិនបានបញ្ចូល"

#: soldevicetypes.cpp:475
#, kde-format
msgid "No Charge"
msgstr "មិនបានបញ្ចូល"

#: soldevicetypes.cpp:478
#, kde-format
msgid "Battery Type: "
msgstr "ប្រភេទ​ថ្ម ៖"

#: soldevicetypes.cpp:478
#, kde-format
msgid "Charge Status: "
msgstr "ស្ថានភាព​បញ្ចូល​ថ្ម ៖"

#: soldevicetypes.cpp:478
#, fuzzy, kde-format
#| msgid "Charge Status: "
msgid "Charge Percent: "
msgstr "ស្ថានភាព​បញ្ចូល​ថ្ម ៖"

#~ msgid "Network Interfaces"
#~ msgstr "ចំណុច​ប្រទាក់​បណ្ដាញ"

#~ msgid "Connected"
#~ msgstr "បាន​តភ្ជាប់"

#~ msgid "Wireless"
#~ msgstr "ឥតខ្សែ"

#~ msgid "Wired"
#~ msgstr "មាន​ខ្សែរ"

#~ msgid "Hardware Address: "
#~ msgstr "អាសយដ្ឋាន​ផ្នែក​រឹង ៖"

#~ msgid "Wireless?"
#~ msgstr "ឥតខ្សែ ?"

#~ msgid "Audio Interfaces"
#~ msgstr "ចំណុច​ប្រទាក់​អូឌីយ៉ូ"

#~ msgid "Alsa Interfaces"
#~ msgstr "ចំណុច​ប្រទាក់ Alsa"

#~ msgid "Open Sound System Interfaces"
#~ msgstr "បើក​ចំណុច​ប្រទាក់​ប្រព័ន្ធ​សំឡេង"

#~ msgid "Control"
#~ msgstr "បញ្ជា"

#~ msgid "Input"
#~ msgstr "ចូល"

#~ msgid "Output"
#~ msgstr "ចេញ"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown audio interface type"
#~ msgid "Unknown"
#~ msgstr "មិន​ស្គាល់"

#~ msgid "Internal Soundcard"
#~ msgstr "កាត​សំឡង​ខាង​ក្នុង"

#~ msgid "USB Soundcard"
#~ msgstr "កាត​សំឡេង USB"

#~ msgid "Firewire Soundcard"
#~ msgstr "កាតសំឡេង Firewire"

#~ msgid "Headset"
#~ msgstr "កាស"

#~ msgid "Modem"
#~ msgstr "ម៉ូដឹម"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown sound card type"
#~ msgid "Unknown"
#~ msgstr "មិន​ស្គាល់"

#~ msgid "Audio Interface Type: "
#~ msgstr "ប្រភេទ​ចំណុច​ប្រទាក់​អូឌីយ៉ូ ៖"

#~ msgid "Soundcard Type: "
#~ msgstr "ប្រភេទ​កាត​សំឡេង ៖"

#~ msgid "Device Buttons"
#~ msgstr "ប៊ុតុង​ឧបករណ៍"

#~ msgid "Lid Button"
#~ msgstr "ប៊ូតុង Lid"

#~ msgid "Power Button"
#~ msgstr "ប៊ូតុង​ថាមពល"

#~ msgid "Sleep Button"
#~ msgstr "ប៊ូតុង​ដេក"

#~ msgid "Tablet Button"
#~ msgstr "ប៊ូតុង Tablet"

#~ msgid "Unknown Button"
#~ msgstr "មិន​ស្គាល់​ប៊ូតុង"

#~ msgid "Button type: "
#~ msgstr "ប្រភេទ​ប៊ូតុង ៖"

#~ msgid "Has State?"
#~ msgstr "មាន​ស្ថានភាព ?"

#~ msgid "AC Adapters"
#~ msgstr "អាដាប់ទ័រ AC"

#~ msgid "Is plugged in?"
#~ msgstr "ត្រូវ​បាន​ដោត ?"

#~ msgid "Digital Video Broadcasting Devices"
#~ msgstr "ឧបករណ៍​ផ្សាយ​វីដេអូ​ឌីជីថល"

#~ msgid "Audio"
#~ msgstr "អូឌីយ៉ូ"

#~ msgid "Conditional access system"
#~ msgstr "ប្រព័ន្ធ​ចូល​ដំណើរការ​តាម​លក្ខខ័ណ្ឌ"

#~ msgid "Demux"
#~ msgstr "Demux"

#~ msgid "Digital video recorder"
#~ msgstr "កម្មវិធី​ថត​វីដេអូ​ឌីជីថល"

#~ msgid "Front end"
#~ msgstr "កម្មវិធី​ខាង​មុខ"

#~ msgid "Network"
#~ msgstr "បណ្ដាញ"

#~ msgid "On-Screen display"
#~ msgstr "ការ​បង្ហាញ​លើ​អេក្រង់"

#~ msgid "Security and content protection"
#~ msgstr "សុវត្ថិភាព និងការ​ការពារ​មាតិកា"

#~ msgid "Video"
#~ msgstr "វីដេអូ"

#~ msgid "Device Type: "
#~ msgstr "ប្រភេទ​ឧបករណ៍ ៖"

#~ msgid "Serial Devices"
#~ msgstr "ឧបករណ៍​សៀរៀល"

#, fuzzy
#~| msgid "Platform"
#~ msgctxt "platform serial interface type"
#~ msgid "Platform"
#~ msgstr "វេទិកា"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown serial interface type"
#~ msgid "Unknown"
#~ msgstr "មិន​ស្គាល់"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown port"
#~ msgid "Unknown"
#~ msgstr "មិន​ស្គាល់"

#~ msgid "Serial Type: "
#~ msgstr "ប្រភេទ​សៀរៀល ៖"

#~ msgid "Port: "
#~ msgstr "ច្រក ៖"

#~ msgid "Smart Card Devices"
#~ msgstr "ឧបករណ៍​កាត​ឆ្លាត"

#~ msgid "Card Reader"
#~ msgstr "កម្មវិធីអាន​កាត"

#~ msgid "Crypto Token"
#~ msgstr "ថូខឹន Crypto"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown smart card type"
#~ msgid "Unknown"
#~ msgstr "មិន​ស្គាល់"

#~ msgid "Smart Card Type: "
#~ msgstr "ប្រភេទ​កាត​​​ឆ្លាត ៖"

#~ msgid "Video Devices"
#~ msgstr "ឧបករណ៍​វីដេអូ"

#~ msgid "Device unable to be cast to correct device"
#~ msgstr "ឧបករណ៍​មិនអាច​ចាត់​ឧបករណ៍​ត្រឹមត្រូវ​ជា​ថ្នាក់​បាន​ទេ"
