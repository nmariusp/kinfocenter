# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kinfocenter package.
#
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kinfocenter\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-30 00:54+0000\n"
"PO-Revision-Date: 2022-12-31 18:10+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "زايد السعيدي"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "zayed.alsaidi@gmail.com"

#: main.cpp:25
#, kde-format
msgctxt "@label kcm name"
msgid "OpenCL"
msgstr "أوبن‌سي‌إل"

#: main.cpp:26
#, kde-format
msgid "Linus Dierheimer"
msgstr "لينوس ديرهايمر"

#: package/contents/ui/main.qml:16
msgctxt "@info"
msgid "OpenCL API Information"
msgstr "معلومات الواجهة البرمجية لأوبن‌سي‌إل"
