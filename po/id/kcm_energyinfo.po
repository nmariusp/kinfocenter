# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Wantoyo <wantoyek@gmail.com>, 2018, 2019, 2022.
# Linerly <linerly@protonmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-16 00:48+0000\n"
"PO-Revision-Date: 2022-10-23 16:58+0700\n"
"Last-Translator: Wantoyèk <wantoyek@gmail.com>\n"
"Language-Team: Indonesian <kde-i18n-doc@kde.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Wantoyo"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wantoyek@gmail.com"

#: kcm.cpp:41
#, kde-format
msgid "Energy Consumption Statistics"
msgstr "Statistik Konsumsi Energi"

#: kcm.cpp:42
#, kde-format
msgid "Kai Uwe Broulik"
msgstr "Kai Uwe Broulik"

#: package/contents/ui/main.qml:22
#, kde-format
msgid "This module lets you see energy information and statistics."
msgstr "Modul ini memungkinkan kamu melihat informasi energi dan statistik."

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Battery"
msgstr "Baterai"

#: package/contents/ui/main.qml:51
#, kde-format
msgid "Rechargeable"
msgstr "Dapat-dicas-ulang"

#: package/contents/ui/main.qml:52
#, kde-format
msgid "Charge state"
msgstr "Kondisi pengecasan"

#: package/contents/ui/main.qml:53
#, kde-format
msgid "Current charge"
msgstr "Cas saat ini"

#: package/contents/ui/main.qml:53 package/contents/ui/main.qml:54
#, kde-format
msgid "%"
msgstr "%"

#: package/contents/ui/main.qml:54
#, kde-format
msgid "Health"
msgstr "Waras"

#: package/contents/ui/main.qml:55
#, kde-format
msgid "Vendor"
msgstr "Vendor"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "Model"
msgstr "Model"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Serial Number"
msgstr "Nomor Seri"

#: package/contents/ui/main.qml:58
#, kde-format
msgid "Technology"
msgstr "Teknologi"

#: package/contents/ui/main.qml:62
#, kde-format
msgid "Energy"
msgstr "Energi"

#: package/contents/ui/main.qml:64
#, kde-format
msgctxt "current power draw from the battery in W"
msgid "Consumption"
msgstr "Konsumsi"

#: package/contents/ui/main.qml:64
#, kde-format
msgctxt "Watt"
msgid "W"
msgstr "W"

#: package/contents/ui/main.qml:65
#, kde-format
msgid "Voltage"
msgstr "Voltase"

#: package/contents/ui/main.qml:65
#, kde-format
msgctxt "Volt"
msgid "V"
msgstr "V"

#: package/contents/ui/main.qml:66
#, kde-format
msgid "Remaining energy"
msgstr "Energi yang tersisa"

#: package/contents/ui/main.qml:66 package/contents/ui/main.qml:67
#: package/contents/ui/main.qml:68
#, kde-format
msgctxt "Watt-hours"
msgid "Wh"
msgstr "Wh"

#: package/contents/ui/main.qml:67
#, kde-format
msgid "Last full charge"
msgstr "Cas penuh terakhir"

#: package/contents/ui/main.qml:68
#, kde-format
msgid "Original charge capacity"
msgstr "Kapasitas cas asli"

#: package/contents/ui/main.qml:72
#, kde-format
msgid "Environment"
msgstr "Lingkungan"

#: package/contents/ui/main.qml:74
#, kde-format
msgid "Temperature"
msgstr "Temperatur"

#: package/contents/ui/main.qml:74
#, kde-format
msgctxt "Degree Celsius"
msgid "°C"
msgstr "°C"

#: package/contents/ui/main.qml:81
#, kde-format
msgid "Not charging"
msgstr "Tidak mengecas"

#: package/contents/ui/main.qml:82
#, kde-format
msgid "Charging"
msgstr "Mengecas"

#: package/contents/ui/main.qml:83
#, kde-format
msgid "Discharging"
msgstr "Nonngecas"

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Fully charged"
msgstr "Tercas penuh"

#: package/contents/ui/main.qml:90
#, kde-format
msgid "Lithium ion"
msgstr "Lithium ion"

#: package/contents/ui/main.qml:91
#, kde-format
msgid "Lithium polymer"
msgstr "Lithium polymer"

#: package/contents/ui/main.qml:92
#, kde-format
msgid "Lithium iron phosphate"
msgstr "Lithium iron phosphate"

#: package/contents/ui/main.qml:93
#, kde-format
msgid "Lead acid"
msgstr "Lead acid"

#: package/contents/ui/main.qml:94
#, kde-format
msgid "Nickel cadmium"
msgstr "Nickel cadmium"

#: package/contents/ui/main.qml:95
#, kde-format
msgid "Nickel metal hydride"
msgstr "Nickel metal hydride"

#: package/contents/ui/main.qml:96
#, kde-format
msgid "Unknown technology"
msgstr "Teknologi tak diketahui"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last hour"
msgstr "Jam terakhir"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 2 hours"
msgstr "2 jam terakhir"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 12 hours"
msgstr "12 jam terakhir"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 24 hours"
msgstr "24 jam terakhir"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 48 hours"
msgstr "48 jam terakhir"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 7 days"
msgstr "7 hari terakhir"

#: package/contents/ui/main.qml:110
#, kde-format
msgctxt "@info:status"
msgid "No energy information available on this system"
msgstr "Tidak ada informasi energi yang tersedia pada sistem ini"

#: package/contents/ui/main.qml:175
#, kde-format
msgid "Internal battery"
msgstr "Baterai internal"

#: package/contents/ui/main.qml:176
#, kde-format
msgid "UPS battery"
msgstr "Baterai UPS"

#: package/contents/ui/main.qml:177
#, kde-format
msgid "Monitor battery"
msgstr "Baterai monitor"

#: package/contents/ui/main.qml:178
#, kde-format
msgid "Mouse battery"
msgstr "Baterai mouse"

#: package/contents/ui/main.qml:179
#, kde-format
msgid "Keyboard battery"
msgstr "Baterai keyboard"

#: package/contents/ui/main.qml:180
#, kde-format
msgid "PDA battery"
msgstr "Baterai PDA"

#: package/contents/ui/main.qml:181
#, kde-format
msgid "Phone battery"
msgstr "Baterai telepon"

#: package/contents/ui/main.qml:182
#, kde-format
msgid "Unknown battery"
msgstr "Baterai tak diketahui"

#: package/contents/ui/main.qml:200
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1% (Charging)"
msgstr "%1% (Mengecas)"

#: package/contents/ui/main.qml:201
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/main.qml:250
#, kde-format
msgctxt "Shorthand for Watts"
msgid "W"
msgstr "W"

#: package/contents/ui/main.qml:250
#, kde-format
msgctxt "literal percent sign"
msgid "%"
msgstr "%"

#: package/contents/ui/main.qml:276
#, kde-format
msgid "Charge Percentage"
msgstr "Persentase Beban"

#: package/contents/ui/main.qml:286
#, kde-format
msgid "Energy Consumption"
msgstr "Konsumsi Energi"

#: package/contents/ui/main.qml:301
#, kde-format
msgid "Timespan"
msgstr "Rentang waktu"

#: package/contents/ui/main.qml:302
#, kde-format
msgid "Timespan of data to display"
msgstr "Rentang waktu data untuk ditampilkan"

#: package/contents/ui/main.qml:308
#, kde-format
msgid "Refresh"
msgstr "Segarkan"

#: package/contents/ui/main.qml:320
#, kde-format
msgid "This type of history is currently not available for this device."
msgstr "Tipe dari histori ini saat ini tidak tersedia untuk peranti ini."

#: package/contents/ui/main.qml:378
#, kde-format
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/main.qml:389
#, kde-format
msgid "Yes"
msgstr "Ya"

#: package/contents/ui/main.qml:391
#, kde-format
msgid "No"
msgstr "Tidak"

#: package/contents/ui/main.qml:409
#, kde-format
msgctxt "%1 is value, %2 is unit"
msgid "%1 %2"
msgstr "%1 %2"

#~ msgid "Application Energy Consumption"
#~ msgstr "Konsumsi Energi Aplikasi"

#~ msgid "Path: %1"
#~ msgstr "Alur: %1"

#~ msgid "PID: %1"
#~ msgstr "PID: %1"

#~ msgid "Wakeups per second: %1 (%2%)"
#~ msgstr "Bangun tiap detik: %1 (%2%)"

#~ msgid "Details: %1"
#~ msgstr "Perincian: %1"

#~ msgid "Manufacturer"
#~ msgstr "Manufaktur"

#~ msgid "Full design"
#~ msgstr "Desain penuh"

#~ msgid "System"
#~ msgstr "Sistem"

#~ msgid "Has power supply"
#~ msgstr "Memiliki suplai daya"

#~ msgid "Capacity"
#~ msgstr "Kapasitas"
