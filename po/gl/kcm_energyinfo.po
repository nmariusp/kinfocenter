# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015, 2017.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-16 00:48+0000\n"
"PO-Revision-Date: 2019-10-19 22:07+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Adrián Chaves Fernández"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "adrian@chaves.io"

#: kcm.cpp:41
#, kde-format
msgid "Energy Consumption Statistics"
msgstr "Estatísticas de consumo enerxético"

#: kcm.cpp:42
#, kde-format
msgid "Kai Uwe Broulik"
msgstr "Kai Uwe Broulik"

#: package/contents/ui/main.qml:22
#, kde-format
msgid "This module lets you see energy information and statistics."
msgstr ""
"Este módulo permítelle consultar a información e as estatísticas de enerxía."

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Battery"
msgstr "Batería"

#: package/contents/ui/main.qml:51
#, kde-format
msgid "Rechargeable"
msgstr "Recargábel"

#: package/contents/ui/main.qml:52
#, kde-format
msgid "Charge state"
msgstr "Estado da carga"

#: package/contents/ui/main.qml:53
#, kde-format
msgid "Current charge"
msgstr "Carga actual"

#: package/contents/ui/main.qml:53 package/contents/ui/main.qml:54
#, kde-format
msgid "%"
msgstr "%"

#: package/contents/ui/main.qml:54
#, kde-format
msgid "Health"
msgstr "Saúde"

#: package/contents/ui/main.qml:55
#, kde-format
msgid "Vendor"
msgstr "Vendedor"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "Model"
msgstr "Modelo"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Serial Number"
msgstr "Número de serie"

#: package/contents/ui/main.qml:58
#, kde-format
msgid "Technology"
msgstr "Tecnoloxía"

#: package/contents/ui/main.qml:62
#, kde-format
msgid "Energy"
msgstr "Enerxía"

#: package/contents/ui/main.qml:64
#, kde-format
msgctxt "current power draw from the battery in W"
msgid "Consumption"
msgstr "Consumo"

#: package/contents/ui/main.qml:64
#, kde-format
msgctxt "Watt"
msgid "W"
msgstr "W"

#: package/contents/ui/main.qml:65
#, kde-format
msgid "Voltage"
msgstr "Voltaxe"

#: package/contents/ui/main.qml:65
#, kde-format
msgctxt "Volt"
msgid "V"
msgstr "V"

#: package/contents/ui/main.qml:66
#, kde-format
msgid "Remaining energy"
msgstr "Enerxía restante"

#: package/contents/ui/main.qml:66 package/contents/ui/main.qml:67
#: package/contents/ui/main.qml:68
#, kde-format
msgctxt "Watt-hours"
msgid "Wh"
msgstr "Wh"

#: package/contents/ui/main.qml:67
#, kde-format
msgid "Last full charge"
msgstr "Última carga completa"

#: package/contents/ui/main.qml:68
#, kde-format
msgid "Original charge capacity"
msgstr "Capacidade de carga orixinal"

#: package/contents/ui/main.qml:72
#, kde-format
msgid "Environment"
msgstr "Ambiente"

#: package/contents/ui/main.qml:74
#, kde-format
msgid "Temperature"
msgstr "Temperatura"

#: package/contents/ui/main.qml:74
#, kde-format
msgctxt "Degree Celsius"
msgid "°C"
msgstr "°C"

#: package/contents/ui/main.qml:81
#, kde-format
msgid "Not charging"
msgstr "Sen cargar"

#: package/contents/ui/main.qml:82
#, kde-format
msgid "Charging"
msgstr "Cargándose"

#: package/contents/ui/main.qml:83
#, kde-format
msgid "Discharging"
msgstr "Descargándose"

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Fully charged"
msgstr "Carga completa"

#: package/contents/ui/main.qml:90
#, kde-format
msgid "Lithium ion"
msgstr "Ión de litio"

#: package/contents/ui/main.qml:91
#, kde-format
msgid "Lithium polymer"
msgstr "Polímero de litio"

#: package/contents/ui/main.qml:92
#, kde-format
msgid "Lithium iron phosphate"
msgstr "Fosfato de litio ferro"

#: package/contents/ui/main.qml:93
#, kde-format
msgid "Lead acid"
msgstr "Chumbo ácido"

#: package/contents/ui/main.qml:94
#, kde-format
msgid "Nickel cadmium"
msgstr "Níquel cadmio"

#: package/contents/ui/main.qml:95
#, kde-format
msgid "Nickel metal hydride"
msgstr "Hidruro de níquel metálico"

#: package/contents/ui/main.qml:96
#, kde-format
msgid "Unknown technology"
msgstr "Tecnoloxía descoñecida"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last hour"
msgstr "Última hora"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 2 hours"
msgstr "Últimas 2 horas"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 12 hours"
msgstr "Últimas 12 horas"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 24 hours"
msgstr "Últimas 24 horas"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 48 hours"
msgstr "Últimas 48 horas"

#: package/contents/ui/main.qml:103
#, kde-format
msgid "Last 7 days"
msgstr "Últimos 7 días"

#: package/contents/ui/main.qml:110
#, kde-format
msgctxt "@info:status"
msgid "No energy information available on this system"
msgstr ""

#: package/contents/ui/main.qml:175
#, kde-format
msgid "Internal battery"
msgstr "Batería interna"

#: package/contents/ui/main.qml:176
#, kde-format
msgid "UPS battery"
msgstr "Batería de SAI"

#: package/contents/ui/main.qml:177
#, kde-format
msgid "Monitor battery"
msgstr "Batería do monitor"

#: package/contents/ui/main.qml:178
#, kde-format
msgid "Mouse battery"
msgstr "Pila do rato"

#: package/contents/ui/main.qml:179
#, kde-format
msgid "Keyboard battery"
msgstr "Pila do teclado"

#: package/contents/ui/main.qml:180
#, kde-format
msgid "PDA battery"
msgstr "Batería de PDA"

#: package/contents/ui/main.qml:181
#, kde-format
msgid "Phone battery"
msgstr "Batería do teléfono"

#: package/contents/ui/main.qml:182
#, kde-format
msgid "Unknown battery"
msgstr "Batería descoñecida"

#: package/contents/ui/main.qml:200
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1% (Charging)"
msgstr "%1% (cargándose)"

#: package/contents/ui/main.qml:201
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/main.qml:250
#, kde-format
msgctxt "Shorthand for Watts"
msgid "W"
msgstr "W"

#: package/contents/ui/main.qml:250
#, kde-format
msgctxt "literal percent sign"
msgid "%"
msgstr "%"

#: package/contents/ui/main.qml:276
#, kde-format
msgid "Charge Percentage"
msgstr "Porcentaxe de carga"

#: package/contents/ui/main.qml:286
#, kde-format
msgid "Energy Consumption"
msgstr "Consumo enerxético"

#: package/contents/ui/main.qml:301
#, kde-format
msgid "Timespan"
msgstr "Duración"

#: package/contents/ui/main.qml:302
#, kde-format
msgid "Timespan of data to display"
msgstr "Duración dos datos que se mostran."

#: package/contents/ui/main.qml:308
#, kde-format
msgid "Refresh"
msgstr "Actualizar"

#: package/contents/ui/main.qml:320
#, kde-format
msgid "This type of history is currently not available for this device."
msgstr ""
"Este tipo de historial non está dispoñíbel actualmente para este dispositivo."

#: package/contents/ui/main.qml:378
#, kde-format
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/main.qml:389
#, kde-format
msgid "Yes"
msgstr "Si"

#: package/contents/ui/main.qml:391
#, kde-format
msgid "No"
msgstr "Non"

#: package/contents/ui/main.qml:409
#, kde-format
msgctxt "%1 is value, %2 is unit"
msgid "%1 %2"
msgstr "%1 %2"

#~ msgid "Application Energy Consumption"
#~ msgstr "Consumo enerxético das aplicacións"

#~ msgid "Path: %1"
#~ msgstr "Ruta: %1"

#~ msgid "PID: %1"
#~ msgstr "PID: %1"

#~ msgid "Wakeups per second: %1 (%2%)"
#~ msgstr "Sacudidas por segundo: %1 (%2%)"

#~ msgid "Details: %1"
#~ msgstr "Detalles: %1"

#~ msgid "Capacity degradation"
#~ msgstr "Degradación da capacidade"

#~ msgid "Manufacturer"
#~ msgstr "Fabricante"

#~ msgid "Full design"
#~ msgstr "Deseño completo"

#~ msgid "System"
#~ msgstr "Sistema"

#~ msgid "Has power supply"
#~ msgstr "Enchufado"

#~ msgid "Capacity"
#~ msgstr "Capacidade"
