# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sairan Kikkarin <sairan@computer.org>, 2010.
# Sairan Kikkarin <sairan(at)computer.org>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-14 00:46+0000\n"
"PO-Revision-Date: 2010-11-21 06:29+0600\n"
"Last-Translator: Sairan Kikkarin <sairan(at)computer.org>\n"
"Language-Team: Kazakh <kde-i18n-doc@kde.org>\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Сайран Киккарин"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sairan@computer.org"

#: networkmodel.cpp:159
#, fuzzy, kde-format
#| msgid "Point to Point"
msgctxt "@item:intable Network type"
msgid "Point to Point"
msgstr "Нүктеден нүктеге"

#: networkmodel.cpp:166
#, fuzzy, kde-format
#| msgctxt "@item:intext Mode of network card"
#| msgid "Broadcast"
msgctxt "@item:intable Network type"
msgid "Broadcast"
msgstr "Кеңтаралым"

#: networkmodel.cpp:173
#, fuzzy, kde-format
#| msgid "Multicast"
msgctxt "@item:intable Network type"
msgid "Multicast"
msgstr "Көпбағытты"

#: networkmodel.cpp:180
#, fuzzy, kde-format
#| msgid "Loopback"
msgctxt "@item:intable Network type"
msgid "Loopback"
msgstr "Бітеуіш"

#: nic.cpp:21
#, kde-format
msgid "kcm_nic"
msgstr ""

#: nic.cpp:22
#, fuzzy, kde-format
#| msgid "Network Mask"
msgctxt "@title:window"
msgid "Network Interfaces"
msgstr "Желі қалқасы"

#: nic.cpp:26
#, fuzzy, kde-format
#| msgid "(c) 2001 - 2002 Alexander Neundorf"
msgctxt "@info"
msgid "(c) 2001 - 2002 Alexander Neundorf"
msgstr "(c) 2001 - 2002 Alexander Neundorf"

#: nic.cpp:28
#, fuzzy, kde-format
#| msgid "Alexander Neundorf"
msgctxt "@info:credit"
msgid "Alexander Neundorf"
msgstr "Alexander Neundorf"

#: nic.cpp:28
#, kde-format
msgctxt "@info:credit"
msgid "creator"
msgstr ""

#: nic.cpp:29
#, kde-format
msgctxt "@info:credit"
msgid "Carl Schwan"
msgstr ""

#: nic.cpp:29
#, kde-format
msgctxt "@info:credit"
msgid "developer"
msgstr ""

#: package/contents/ui/main.qml:15
#, fuzzy, kde-format
#| msgid "Network Mask"
msgid "Network Interfaces"
msgstr "Желі қалқасы"

#: package/contents/ui/main.qml:30
#, fuzzy, kde-format
#| msgid "Name"
msgctxt "@label"
msgid "Name:"
msgstr "Атауы"

#: package/contents/ui/main.qml:34
#, fuzzy, kde-format
#| msgid "IP Address"
msgctxt "@label"
msgid "Address:"
msgstr "IP адресі"

#: package/contents/ui/main.qml:38
#, fuzzy, kde-format
#| msgid "Network Mask"
msgctxt "@label"
msgid "Network Mask:"
msgstr "Желі қалқасы"

#: package/contents/ui/main.qml:42
#, fuzzy, kde-format
#| msgid "Type"
msgctxt "@label"
msgid "Type:"
msgstr "Түрі"

#: package/contents/ui/main.qml:46
#, kde-format
msgctxt "@label"
msgid "Hardware Address:"
msgstr ""

#: package/contents/ui/main.qml:51
#, fuzzy, kde-format
#| msgid "State"
msgctxt "@label"
msgid "State:"
msgstr "Күй-жайы"

#: package/contents/ui/main.qml:59
#, kde-format
msgctxt "State of network card is connected"
msgid "Up"
msgstr "Бар"

#: package/contents/ui/main.qml:60
#, kde-format
msgctxt "State of network card is disconnected"
msgid "Down"
msgstr "Жоқ"

#: package/contents/ui/main.qml:76
#, kde-format
msgctxt "@action:button"
msgid "Refresh"
msgstr ""

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Broadcast"
#~ msgstr "Кеңтаралым"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Point to Point"
#~ msgstr "Нүктеден нүктеге"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Multicast"
#~ msgstr "Көпбағытты"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Loopback"
#~ msgstr "Бітеуіш"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Unknown"
#~ msgstr "Беймәлім"

#~ msgctxt "Unknown network mask"
#~ msgid "Unknown"
#~ msgstr "Беймәлім"

#~ msgctxt "Unknown HWaddr"
#~ msgid "Unknown"
#~ msgstr "Беймәлім"

#, fuzzy
#~| msgid "Broadcast"
#~ msgctxt "@item:intable Netork type"
#~ msgid "Broadcast"
#~ msgstr "Кейтаралым"

#~ msgid "HWAddr"
#~ msgstr "ЖБАдресі"

#~ msgid "&Update"
#~ msgstr "&Жаңарту"

#~ msgid "kcminfo"
#~ msgstr "kcminfo"

#~ msgid "System Information Control Module"
#~ msgstr "Жүйелік мәліметін басқару модулі"
