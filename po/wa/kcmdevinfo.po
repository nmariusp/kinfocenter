# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Jean Cayron <jean.cayron@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-16 00:48+0000\n"
"PO-Revision-Date: 2010-12-24 17:42+0100\n"
"Last-Translator: Jean Cayron <jean.cayron@gmail.com>\n"
"Language-Team: Walloon <linux@walon.org>\n"
"Language: wa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Djan Cayron"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "jean.cayron@gmail.com"

#: devicelisting.cpp:41
#, kde-format
msgctxt "Device Listing Whats This"
msgid "Shows all the devices that are currently listed."
msgstr "Mostere tos les éndjins ki sont-st el djivêye pol moumint."

#: devicelisting.cpp:44
#, kde-format
msgid "Devices"
msgstr "Éndjins"

#: devicelisting.cpp:57
#, kde-format
msgid "Collapse All"
msgstr "Totafwait catchî"

#: devicelisting.cpp:60
#, kde-format
msgid "Expand All"
msgstr "Totafwait mostrer"

#: devicelisting.cpp:63
#, kde-format
msgid "Show All Devices"
msgstr "Mostrer tos les éndjins"

#: devicelisting.cpp:66
#, kde-format
msgid "Show Relevant Devices"
msgstr "Mostere les éndjins ahessants"

#: devicelisting.cpp:95
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown device type"
msgid "Unknown"
msgstr "Nén cnoxhou"

#: devicelisting.cpp:136 devinfo.cpp:79
#, fuzzy, kde-format
#| msgid "None"
msgctxt "no device UDI"
msgid "None"
msgstr "Nouk"

#: devinfo.cpp:28
#, kde-format
msgid "kcmdevinfo"
msgstr "kcmdevinfo"

#: devinfo.cpp:28
#, fuzzy, kde-format
#| msgid "KDE Solid Based Device Viewer"
msgid "Device Viewer"
msgstr "Håyneu des éndjins da KDE båzé so Solid"

#: devinfo.cpp:28
#, kde-format
msgid "(c) 2010 David Hubner"
msgstr "© 2010 David Hubner"

#: devinfo.cpp:58
#, kde-format
msgid "UDI: "
msgstr "UDI : "

#: devinfo.cpp:66
#, kde-format
msgctxt "Udi Whats This"
msgid "Shows the current device's UDI (Unique Device Identifier)"
msgstr ""
"Mostere l' UDI (Idintifieu Unike da l' Éndjin - Unique Device Identifier) da "
"l' éndjin do moumint"

#: infopanel.cpp:20
#, kde-format
msgid "Device Information"
msgstr "Infôrmåcion so l' éndjin"

#: infopanel.cpp:29
#, kde-format
msgctxt "Info Panel Whats This"
msgid "Shows information about the currently selected device."
msgstr "Mostere les informåcions åd fwait di l' éndjin tchoezi pol moumint."

#: infopanel.cpp:56
#, kde-format
msgid ""
"\n"
"Solid Based Device Viewer Module"
msgstr ""
"\n"
"Module di håynaedje des éndjins båzé so Solid"

#: infopanel.cpp:119
#, kde-format
msgid "Description: "
msgstr ""

#: infopanel.cpp:121
#, kde-format
msgid "Product: "
msgstr "Prodût : "

#: infopanel.cpp:123
#, kde-format
msgid "Vendor: "
msgstr "Vindeu : "

#: infopanel.cpp:145
#, kde-format
msgid "Yes"
msgstr "Oyi"

#: infopanel.cpp:147
#, kde-format
msgid "No"
msgstr "Neni"

#: infopanel.h:36
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "name of something is not known"
msgid "Unknown"
msgstr "Nén cnoxhou"

#: soldevice.cpp:65
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown device"
msgid "Unknown"
msgstr "Nén cnoxhou"

#: soldevice.cpp:91
#, kde-format
msgctxt "Default device tooltip"
msgid "A Device"
msgstr "Èn éndjin"

#: soldevicetypes.cpp:43
#, kde-format
msgid "Processors"
msgstr "Processeus"

#: soldevicetypes.cpp:59
#, kde-format
msgid "Processor %1"
msgstr "Processeu %1"

#: soldevicetypes.cpp:76
#, kde-format
msgid "Intel MMX"
msgstr "Intel MMX"

#: soldevicetypes.cpp:79
#, kde-format
msgid "Intel SSE"
msgstr "Intel SSE"

#: soldevicetypes.cpp:82
#, kde-format
msgid "Intel SSE2"
msgstr "Intel SSE2"

#: soldevicetypes.cpp:85
#, kde-format
msgid "Intel SSE3"
msgstr "Intel SSE3"

#: soldevicetypes.cpp:88
#, fuzzy, kde-format
#| msgid "Intel SSE3"
msgid "Intel SSSE3"
msgstr "Intel SSE3"

#: soldevicetypes.cpp:91
#, fuzzy, kde-format
#| msgid "Intel SSE4"
msgid "Intel SSE4.1"
msgstr "Intel SSE4"

#: soldevicetypes.cpp:94
#, fuzzy, kde-format
#| msgid "Intel SSE4"
msgid "Intel SSE4.2"
msgstr "Intel SSE4"

#: soldevicetypes.cpp:97
#, fuzzy, kde-format
#| msgid "AMD 3DNow"
msgid "AMD 3DNow!"
msgstr "AMD 3DNow"

#: soldevicetypes.cpp:100
#, kde-format
msgid "ATI IVEC"
msgstr "ATI IVEC"

#: soldevicetypes.cpp:103
#, fuzzy, kde-format
#| msgid "None"
msgctxt "no instruction set extensions"
msgid "None"
msgstr "Nouk"

#: soldevicetypes.cpp:106
#, kde-format
msgid "Processor Number: "
msgstr "Limero do processeu : "

#: soldevicetypes.cpp:106
#, kde-format
msgid "Max Speed: "
msgstr "Raddisté macsimom : "

#: soldevicetypes.cpp:107
#, kde-format
msgid "Supported Instruction Sets: "
msgstr "Djeus d' instruccions sopoirtés : "

#: soldevicetypes.cpp:132
#, kde-format
msgid "Storage Drives"
msgstr "Léjheus d' sitocaedjes"

#: soldevicetypes.cpp:151
#, kde-format
msgid "Hard Disk Drive"
msgstr "Léjheu d' deure plake"

#: soldevicetypes.cpp:154
#, kde-format
msgid "Compact Flash Reader"
msgstr "Léjheu d' Flash rastrindou"

#: soldevicetypes.cpp:157
#, kde-format
msgid "Smart Media Reader"
msgstr "Léjheu d' media Smart"

#: soldevicetypes.cpp:160
#, kde-format
msgid "SD/MMC Reader"
msgstr "Léjheu SD/MMC"

#: soldevicetypes.cpp:163
#, kde-format
msgid "Optical Drive"
msgstr "Léjheu optike"

#: soldevicetypes.cpp:166
#, kde-format
msgid "Memory Stick Reader"
msgstr "Léjheu d' clé di memwere"

#: soldevicetypes.cpp:169
#, kde-format
msgid "xD Reader"
msgstr "Léjheu xD"

#: soldevicetypes.cpp:172
#, kde-format
msgid "Unknown Drive"
msgstr "Léjheu nén cnoxhou"

#: soldevicetypes.cpp:192
#, kde-format
msgid "IDE"
msgstr "IDE"

#: soldevicetypes.cpp:195
#, kde-format
msgid "USB"
msgstr "USB"

#: soldevicetypes.cpp:198
#, kde-format
msgid "IEEE1394"
msgstr "IEEE1394"

#: soldevicetypes.cpp:201
#, kde-format
msgid "SCSI"
msgstr "SCSI"

#: soldevicetypes.cpp:204
#, kde-format
msgid "SATA"
msgstr "SATA"

#: soldevicetypes.cpp:207
#, fuzzy, kde-format
#| msgid "Platform"
msgctxt "platform storage bus"
msgid "Platform"
msgstr "Platfôme"

#: soldevicetypes.cpp:210
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown storage bus"
msgid "Unknown"
msgstr "Nén cnoxhou"

#: soldevicetypes.cpp:213
#, kde-format
msgid "Bus: "
msgstr "Bus : "

#: soldevicetypes.cpp:213
#, kde-format
msgid "Hotpluggable?"
msgstr "Tchôdmint tchôcåve ?"

#: soldevicetypes.cpp:213
#, kde-format
msgid "Removable?"
msgstr "Oiståve ?"

#: soldevicetypes.cpp:257
#, kde-format
msgid "Unused"
msgstr "Nén eployî"

#: soldevicetypes.cpp:260
#, kde-format
msgid "File System"
msgstr "Sistinme di fitchî"

#: soldevicetypes.cpp:263
#, kde-format
msgid "Partition Table"
msgstr "Tåve des pårticions"

#: soldevicetypes.cpp:266
#, kde-format
msgid "Raid"
msgstr "Raid"

#: soldevicetypes.cpp:269
#, kde-format
msgid "Encrypted"
msgstr "Ecripté"

#: soldevicetypes.cpp:272
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown volume usage"
msgid "Unknown"
msgstr "Nén cnoxhou"

#: soldevicetypes.cpp:275
#, kde-format
msgid "File System Type: "
msgstr "Sôre di sistinme di fitchî : "

#: soldevicetypes.cpp:275
#, kde-format
msgid "Label: "
msgstr "No : "

#: soldevicetypes.cpp:276
#, kde-format
msgid "Not Set"
msgstr "Nén defineye"

#: soldevicetypes.cpp:276
#, kde-format
msgid "Volume Usage: "
msgstr "Eployaedje do volume : "

#: soldevicetypes.cpp:276
#, kde-format
msgid "UUID: "
msgstr "UUID : "

#: soldevicetypes.cpp:282
#, kde-format
msgid "Mounted At: "
msgstr "Monté a : "

#: soldevicetypes.cpp:282
#, kde-format
msgid "Not Mounted"
msgstr "Nén monté"

#: soldevicetypes.cpp:287
#, kde-format
msgid "Volume Space:"
msgstr "Plaece sol volume :"

#: soldevicetypes.cpp:297
#, kde-format
msgctxt "Available space out of total partition size (percent used)"
msgid "%1 free of %2 (%3% used)"
msgstr "%1 libe so %2 (%3% d' eployî)"

#: soldevicetypes.cpp:303
#, kde-format
msgid "No data available"
msgstr ""

#: soldevicetypes.cpp:330
#, kde-format
msgid "Multimedia Players"
msgstr "Djouweus multimedia"

#: soldevicetypes.cpp:349 soldevicetypes.cpp:388
#, kde-format
msgid "Supported Drivers: "
msgstr "Mineus sopoirtés : "

#: soldevicetypes.cpp:349 soldevicetypes.cpp:388
#, kde-format
msgid "Supported Protocols: "
msgstr "Protocoles sopoirtés : "

#: soldevicetypes.cpp:369
#, kde-format
msgid "Cameras"
msgstr "Cameras"

#: soldevicetypes.cpp:408
#, kde-format
msgid "Batteries"
msgstr "Batreyes"

#: soldevicetypes.cpp:430
#, kde-format
msgid "PDA"
msgstr "PDA"

#: soldevicetypes.cpp:433
#, kde-format
msgid "UPS"
msgstr "UPS"

#: soldevicetypes.cpp:436
#, kde-format
msgid "Primary"
msgstr "Prumioûle"

#: soldevicetypes.cpp:439
#, kde-format
msgid "Mouse"
msgstr "Sori"

#: soldevicetypes.cpp:442
#, kde-format
msgid "Keyboard"
msgstr "Taprece"

#: soldevicetypes.cpp:445
#, kde-format
msgid "Keyboard + Mouse"
msgstr "Taprece + sori"

#: soldevicetypes.cpp:448
#, kde-format
msgid "Camera"
msgstr "Camera"

#: soldevicetypes.cpp:451
#, kde-format
msgid "Phone"
msgstr ""

#: soldevicetypes.cpp:454
#, kde-format
msgctxt "Screen"
msgid "Monitor"
msgstr ""

#: soldevicetypes.cpp:457
#, kde-format
msgctxt "Wireless game pad or joystick battery"
msgid "Gaming Input"
msgstr ""

#: soldevicetypes.cpp:460
#, fuzzy, kde-format
#| msgid "Unknown"
msgctxt "unknown battery type"
msgid "Unknown"
msgstr "Nén cnoxhou"

#: soldevicetypes.cpp:466
#, kde-format
msgid "Charging"
msgstr "Si tchedje"

#: soldevicetypes.cpp:469
#, kde-format
msgid "Discharging"
msgstr "Si distchedje"

#: soldevicetypes.cpp:472
#, fuzzy, kde-format
#| msgid "No Charge"
msgid "Fully Charged"
msgstr "Nou tcherdjaedje"

#: soldevicetypes.cpp:475
#, kde-format
msgid "No Charge"
msgstr "Nou tcherdjaedje"

#: soldevicetypes.cpp:478
#, kde-format
msgid "Battery Type: "
msgstr "Sôre di batreye : "

#: soldevicetypes.cpp:478
#, kde-format
msgid "Charge Status: "
msgstr "Estat del tchedje : "

#: soldevicetypes.cpp:478
#, fuzzy, kde-format
#| msgid "Charge Status: "
msgid "Charge Percent: "
msgstr "Estat del tchedje : "

#~ msgid "Network Interfaces"
#~ msgstr "Eterfaces rantoele"

#~ msgid "Connected"
#~ msgstr "Raloyî"

#~ msgid "Wireless"
#~ msgstr "Sins fyi"

#~ msgid "Wired"
#~ msgstr "Avou fyi"

#~ msgid "Hardware Address: "
#~ msgstr "Adresse di l' éndjolreye : "

#~ msgid "Wireless?"
#~ msgstr "Sins fyi ?"

#~ msgid "Audio Interfaces"
#~ msgstr "Eterfaces odio"

#~ msgid "Alsa Interfaces"
#~ msgstr "Eterfaces Alsa"

#~ msgid "Open Sound System Interfaces"
#~ msgstr "Eterfaces Open Sound System"

#~ msgid "Control"
#~ msgstr "Controle"

#~ msgid "Input"
#~ msgstr "Intrêye"

#~ msgid "Output"
#~ msgstr "Rexhowe"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown audio interface type"
#~ msgid "Unknown"
#~ msgstr "Nén cnoxhou"

#~ msgid "Internal Soundcard"
#~ msgstr "Divintrinne cwåte son"

#~ msgid "USB Soundcard"
#~ msgstr "Cwåte son USB"

#~ msgid "Firewire Soundcard"
#~ msgstr "Cwåte son Firewire"

#~ msgid "Headset"
#~ msgstr "Casse odio"

#~ msgid "Modem"
#~ msgstr "Houcrece"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown sound card type"
#~ msgid "Unknown"
#~ msgstr "Nén cnoxhou"

#~ msgid "Audio Interface Type: "
#~ msgstr "Sôre d' eterface di son : "

#~ msgid "Soundcard Type: "
#~ msgstr "Sôre di cwåte son : "

#~ msgid "Device Buttons"
#~ msgstr "Botons d' éndjin"

#~ msgid "Lid Button"
#~ msgstr "Boutoe do covieke"

#~ msgid "Power Button"
#~ msgstr "Boutoe d' aloumaedje"

#~ msgid "Sleep Button"
#~ msgstr "Boutoe d' edoirmaedje"

#~ msgid "Tablet Button"
#~ msgstr "Boutoe del tåvlete"

#~ msgid "Unknown Button"
#~ msgstr "Boutoe nén cnoxhou"

#~ msgid "Button type: "
#~ msgstr "Sôre di boutoe : "

#~ msgid "Has State?"
#~ msgstr "A-st i l' estat ?"

#~ msgid "AC Adapters"
#~ msgstr "Adapteus AC"

#~ msgid "Is plugged in?"
#~ msgstr "Est-i tchôkî ?"

#~ msgid "Digital Video Broadcasting Devices"
#~ msgstr "Éndjins d' sipårdaedje di videyos limerikes"

#~ msgid "Audio"
#~ msgstr "Son"

#~ msgid "Conditional access system"
#~ msgstr "Sistinme d' accès condicionel"

#~ msgid "Demux"
#~ msgstr "Demux"

#~ msgid "Digital video recorder"
#~ msgstr "Eredjistreu d' videyo limerike"

#~ msgid "Front end"
#~ msgstr "Eterface uzeu"

#~ msgid "Network"
#~ msgstr "Rantoele"

#~ msgid "On-Screen display"
#~ msgstr "Håynaedje sol waitroûle (OSD)"

#~ msgid "Security and content protection"
#~ msgstr "Såvrité eyet waerantixhaedje di l' ådvins"

#~ msgid "Video"
#~ msgstr "Videyo"

#~ msgid "Device Type: "
#~ msgstr "Sôre d' éndjin : "

#~ msgid "Serial Devices"
#~ msgstr "Éndjins séreye"

#, fuzzy
#~| msgid "Platform"
#~ msgctxt "platform serial interface type"
#~ msgid "Platform"
#~ msgstr "Platfôme"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown serial interface type"
#~ msgid "Unknown"
#~ msgstr "Nén cnoxhou"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown port"
#~ msgid "Unknown"
#~ msgstr "Nén cnoxhou"

#~ msgid "Serial Type: "
#~ msgstr "Sôre del séreye : "

#~ msgid "Port: "
#~ msgstr "Pôrt : "

#~ msgid "Smart Card Devices"
#~ msgstr "Éndjins d' cwåte Smart"

#~ msgid "Card Reader"
#~ msgstr "Léjheu d' cwåte"

#~ msgid "Crypto Token"
#~ msgstr "Componint d' criptografeye"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown smart card type"
#~ msgid "Unknown"
#~ msgstr "Nén cnoxhou"

#~ msgid "Smart Card Type: "
#~ msgstr "Sôre di cwåte Smart : "

#~ msgid "Video Devices"
#~ msgstr "Éndjins videyo"

#~ msgid "Device unable to be cast to correct device"
#~ msgstr "Dji n' sai covierti l' éndjin e boun éndjin comifåt"
