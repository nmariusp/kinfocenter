# translation of kcmsamba.po to Kurdish
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Erdal Ronahi <erdal.ronahi@nospam.gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: kcmsamba\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-17 00:29+0000\n"
"PO-Revision-Date: 2008-12-25 12:19+0200\n"
"Last-Translator: Omer Ensari <oensari@gmail.com>\n"
"Language-Team: Kurdish <ubuntu-l10n-kur@lists.ubuntu.com>\n"
"Language: ku\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Language: Kurdish\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Omer Ensari"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "oensari@gmail.com"

#: main.cpp:25
#, kde-format
msgid "kcmsamba"
msgstr "kcmsamba"

#: main.cpp:26
#, kde-format
msgid "Samba Status"
msgstr ""

#: main.cpp:30
#, fuzzy, kde-format
#| msgid "(c) 2002 KDE Information Control Module Samba Team"
msgid "(c) 2002-2020 KDE Information Control Module Samba Team"
msgstr "(c) 2002 KDE Information Control Module Samba Team"

#: main.cpp:31
#, kde-format
msgid "Michael Glauche"
msgstr "Michael Glauche"

#: main.cpp:32
#, kde-format
msgid "Matthias Hoelzer"
msgstr "Matthias Hoelzer"

#: main.cpp:33
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: main.cpp:34
#, kde-format
msgid "Harald Koschinski"
msgstr "Harald Koschinski"

#: main.cpp:35
#, kde-format
msgid "Wilco Greven"
msgstr "Wilco Greven"

#: main.cpp:36
#, kde-format
msgid "Alexander Neundorf"
msgstr "Alexander Neundorf"

#: main.cpp:37
#, kde-format
msgid "Harald Sitter"
msgstr ""

#: package/contents/ui/main.qml:17
#, kde-format
msgctxt "@title heading above listview"
msgid "User-Created Shares"
msgstr ""

#: package/contents/ui/main.qml:22
#, kde-format
msgctxt "@title heading above listview"
msgid "Mounted Remote Shares"
msgstr ""

#: package/contents/ui/main.qml:49
#, kde-format
msgctxt "@info place holder for empty listview"
msgid "There are no directories shared by users"
msgstr ""

#: package/contents/ui/main.qml:89
#, kde-format
msgctxt "@info place holder for empty listview"
msgid "There are no Samba shares mounted on this system"
msgstr ""

#: package/contents/ui/ShareListItem.qml:71
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid "Open folder properties to change share settings"
msgstr ""

#, fuzzy
#~| msgid "(c) 2002 KDE Information Control Module Samba Team"
#~ msgid "System Information Control Module"
#~ msgstr "(c) 2002 KDE Information Control Module Samba Team"

#~ msgid "Resource"
#~ msgstr "Çavkanî"

#~ msgid "Type"
#~ msgstr "Cure"

#~ msgid "Show opened connections"
#~ msgstr "Girêdanên vekirî nîşan bide"

#~ msgid "Show closed connections"
#~ msgstr "Girêdanên girtî nîşan bide"

#~ msgid "Show opened files"
#~ msgstr "Pelên vekirî nîşan bide"

#~ msgid "Show closed files"
#~ msgstr "Pelên girtî nîşan bide"

#~ msgid "&Update"
#~ msgstr "&Rojanekirin"

#~ msgid "Date & Time"
#~ msgstr "Dîrok & Dem"

#~ msgid "Event"
#~ msgstr "Bûyer"

#~ msgid "Service/File"
#~ msgstr "Servîs/Pel"

#~ msgid "CONNECTION OPENED"
#~ msgstr "GIRÊDAN HATE VEKIRIN"

#~ msgid "CONNECTION CLOSED"
#~ msgstr "GIRÊDAN HATE GIRTIN"

#~ msgid "            FILE OPENED"
#~ msgstr "            PEL HATE VEKIRIN"

#~ msgid "            FILE CLOSED"
#~ msgstr "           PEL HATE GIRTIN"

#~ msgid "Could not open file %1"
#~ msgstr "Nikare pela %1 veke"

#~ msgid "Connections: 0"
#~ msgstr "Girêdan: 0"

#~ msgid "File accesses: 0"
#~ msgstr "Pelên hatine gihîştin: 0"

#~ msgid "Event: "
#~ msgstr "Bûyer:"

#~ msgid "Service/File:"
#~ msgstr "Servîs/Pel:"

#~ msgid "&Search"
#~ msgstr "&Lêgere"

#~ msgid "Clear Results"
#~ msgstr "Encaman Paqij Bike"

#~ msgid "Nr"
#~ msgstr "Nr"

#~ msgid "Connection"
#~ msgstr "Girêdan"

#~ msgid "File Access"
#~ msgstr "Gihîştina Pelê"

#~ msgid "Connections: %1"
#~ msgstr "Girêdan: %1"

#~ msgid "File accesses: %1"
#~ msgstr "Pelên hatine gihîştin: %1"

#~ msgid "FILE OPENED"
#~ msgstr "PEL HATE VEKIRIN"

#~ msgid "Service"
#~ msgstr "Servîs"

#~ msgid "UID"
#~ msgstr "UID"

#~ msgid "GID"
#~ msgstr "GID"

#~ msgid "PID"
#~ msgstr "PID"

#~ msgid "Open Files"
#~ msgstr "Pelan Veke"

#~ msgid "&Statistics"
#~ msgstr "&Îstatîstîk"
